var path = require('path');
var webpack = require('webpack');

module.exports = {

  devtool: 'eval',

  entry: [
    'webpack-dev-server/client?http://0.0.0.0:3000',
    'webpack/hot/only-dev-server',
    'react-hot-loader/patch',
    'whatwg-fetch',
    './src/index'
  ],

  output: {
    path: path.resolve(__dirname),
    filename: 'app.js'
  },

  module: {
    preLoaders: [{
        test: /\.js?$/,
        loaders: ['eslint'],
        exclude: /node_modules/,
        include: path.join(__dirname, 'src')
      }
    ],
    loaders: [{
        test: /\.js$/,
        loaders: ['babel'],
        exclude: /node_modules/,
        include: path.join(__dirname, 'src')
      }, {
        test: /\.css$/,
        loaders: [
          'style?sourceMap',
          'css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]'
        ]
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]

};
