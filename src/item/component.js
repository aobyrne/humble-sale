import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../app/actions';
import CSSModules from 'react-css-modules';
import styles from './style.css';

class Item extends React.Component {

  constructor(props) {
    super(props);
    this._onClick = this._onClick.bind(this);
  }

  render() {
    const { title, price, discount, src, href } = this.props.item;
    return (
      <div styleName="container">
        <img src={src} title={title} onClick={() => this._onClick(this.props.item)}/>
        <span>{discount}</span>
        <a href={href} target="_blank">
          <span>{price}</span>
        </a>
      </div>
    );
  }

  _onClick(item) {
    this.props.viewItem(item);
  }

}

const mapActionsToProps = (dispatch) => {
  return {
    viewItem: (item) => {
      dispatch(actions.viewItem(item));
    }
  };
};

export default connect(
  null,
  mapActionsToProps
)(CSSModules(Item, styles));
