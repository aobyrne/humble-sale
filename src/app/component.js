import React from 'react';
import { Provider, connect } from 'react-redux';
import CSSModules from 'react-css-modules';
import styles from './style.css';
import { component as Items } from '../items';
import { component as ItemDetails } from '../item-details';

const component = ({ store, item }) => (
  <Provider store={store}>
    <div styleName="app">
      <div styleName="items">
        <Items/>
      </div>
      {item.id &&
        <ItemDetails/>
      }
    </div>
  </Provider>
);

const mapStateToProps = (state) => {
  return {
    item: state.item
  };
};

export default connect(
  mapStateToProps
)(CSSModules(component, styles));
