let nextItemId = 0;

export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const SET_ITEMS = 'SET_ITEMS';
export const VIEW_ITEM = 'VIEW_ITEM';

export function addItem(value) {
  return { type:ADD_ITEM, id:nextItemId++, value };
}

export function removeItem(value) {
  return { type:REMOVE_ITEM, value };
}

export function setItems(value) {
  return { type:SET_ITEMS, value };
}

export function viewItem(value) {
  return { type:VIEW_ITEM, value };
}
