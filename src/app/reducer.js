import { combineReducers } from 'redux';
import { ADD_ITEM, REMOVE_ITEM, SET_ITEMS, VIEW_ITEM } from './actions';

const itemReducer = (state=[], action) => {
  switch (action.type) {
    case VIEW_ITEM:
      return action.value;
    default:
      return state;
  }
};

const itemsReducer = (state=[], action) => {
  switch (action.type) {
    case ADD_ITEM:
      return [
        ...state, {
          id: action.id,
          text: action.value
        }
      ];
    case REMOVE_ITEM:
      return state.filter(item =>
        item.id !== action.value
      );
    case SET_ITEMS:
      return action.value;
    default:
      return state;
  }
};


export default combineReducers({
  item: itemReducer,
  items: itemsReducer
});
