import { createStore } from 'redux';
import reducers from './reducer';

const devTools = window.devToolsExtension ? window.devToolsExtension() : undefined;

const initialState = {
  items: [],
  item: {}
};

function configureStore () {
  const store = createStore(reducers, initialState, devTools);
  if (module.hot) {
    module.hot.accept('./reducer.js', () =>
      store.replaceReducer(require('./reducer.js').default)
    );
  }
  return store;
}

export default configureStore;
