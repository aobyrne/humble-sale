import Cache from './cache';

const API_URL = 'https://extraction.import.io/query/extractor/9ed59ee7-05fe-4b9a-9727-10d501f8ef3d?_apikey=44b2c2a5d8ae449785e70e4cd46d335f8f841b1dbf040b0262cb2f8403cbaf0b8e7b8edae59e33b45c6efba0158e2851cbf82db3e16c35943483215405afc8d0cb1e597e8c0fd55a3227c199cc1d0d19&url=https://www.humblebundle.com/store/promo/onsalethisweek/';

const cache = Cache('humble-items');

const humble = {
  get: () => {
    return new Promise(
      (resolve) => {
        fetch(API_URL)
          .then(r => r.json())
          .then(result => {
            const group = result.extractorData.data[0].group;
            const items = group.map((item, index) => {
              return {
                'id': index,
                'title': item.Title[0].text,
                'discount': item.Discount[0].text,
                'price': item.Price[0].text,
                'src': item.Image[0].src,
                'href': item.Image[0].href
              };
            });
            resolve(items);
          });
      }
    );
  }
};

const api = {
  list: () => {
    return new Promise(
      (resolve) => {
        let cachedItems = cache.get();
        if (!!cachedItems && cachedItems.length) {
          resolve(cachedItems);
        } else {
          humble.get().then((items) => {
            cache.set(items);
            resolve(items);
          });
        }
      }
    );
  }
};

export default api;
