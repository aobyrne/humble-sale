const service = (key) => {
  const API_URL = 'https://www.googleapis.com/youtube/v3/search';
  const API_KEY = key;

  const search = (query) => {
    return new Promise((resolve) => {
      const url = `${API_URL}?q=${query}&key=${API_KEY}&part=snippet`;
      fetch(url)
        .then(r => r.json())
        .then(result => {
          const results = result.items.map((video) => video.id.videoId);
          resolve(results);
        });
    });
  };

  const findVideo = (query) => {
    return new Promise((resolve) => {
      search(query)
        .then((results) => {
          var result = results.find(result => result !== undefined);
          resolve(result);
        });
    });
  };

  return {
    findVideo: findVideo
  };
};

export default service;
