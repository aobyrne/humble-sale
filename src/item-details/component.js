import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../app/actions';
import CSSModules from 'react-css-modules';
import styles from './style.css';
import YouTube from '../utils/youtube';

const YOUTUBE_API_KEY = 'AIzaSyDpyw-KL-a4YVnlAU0YcUuViUfFq_nv7BA';
const youtubeService = YouTube(YOUTUBE_API_KEY);

class ItemDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      videoId: ''
    };
  }

  render() {
    const { title } = this.props.item;
    const { videoId } = this.state;
    const src = `//www.youtube.com/embed/${videoId}?autoplay=1`;
    if (title) {
      return (
        <div styleName="container">
          <h1>{title}</h1>
          <a href="#" onClick={() => this._onClickClose()} styleName="close"></a>
          <iframe width="560" height="315" src={src} frameborder="0" allowfullscreen></iframe>
        </div>
      );
    } else {
      return (
        <div></div>
      );
    }
  }

  componentDidMount() {
    const { title } = this.props.item;
    youtubeService
      .findVideo(`${title} game trailer`)
      .then((result) => {
        this.setState({
          videoId: result
        });
      });
  }

  _onClickClose() {
    this.props.viewItem({});
  }

}

const mapStateToProps = (state) => {
  return {
    item: state.item
  };
};

const mapActionsToProps = (dispatch) => {
  return {
    viewItem: (item) => {
      dispatch(actions.viewItem(item));
    }
  };
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(CSSModules(ItemDetails, styles));
