import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../app/actions';
import { component as Item } from '../item/index';
import CSSModules from 'react-css-modules';
import styles from './style.css';
import HumbleBundle from '../utils/humble-bundle';

class Items extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loaded: false
    };
  }

  render() {
    const { items } = this.props;
    const { loaded } = this.state;
    return (
      !loaded ?
        <div>Loading...</div> :
        <div styleName="container">
          {items.map(item =>
              <Item key={item.id} item={item}/>
          )}
        </div>
    );
  }

  componentDidMount() {
    HumbleBundle.list().then((items) => {
      this.setState({
        loaded: true
      });
      this.props.setItems(items);
    });
  }

}

const mapStateToProps = (state) => {
  return {
    items: state.items
  };
};

const mapActionsToProps = (dispatch) => {
  return {
    setItems: (items) => {
      dispatch(actions.setItems(items));
    }
  };
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(CSSModules(Items, styles));
