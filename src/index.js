import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './app/component';
import configureStore from './app/store';

const store = configureStore();

const el = document.createElement('div');
document.body.appendChild(el);

render(
  <AppContainer>
    <App store={store}/>
  </AppContainer>,
  el
);

if (module.hot) {
  module.hot.accept('./app/component', () => {
    const DefaultApp = require('./app/component').default;
    render(
      <AppContainer>
        <DefaultApp store={store}/>
      </AppContainer>,
      el
    );
  });
}
